﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Claculator_GUI.BL
{
    class Cal
    {
        private float no1;
        private char sign;
        private float no2;

        public char Sign { 
            get => sign;
            set => sign = value; }

        public Cal(float no1, char sign, float no2)
        {
            this.no1 = no1;
            this.Sign = sign;
            this.no2 = no2;
        }

        public  float getno1()
        {
            return no1;
        }

        public void  setno1(float no1)
        {
            this.no1 = no1;
        }
        public float getno2()
        {
            return no2;
        }

        public void setno2(float no2)
        {
            this.no2 = no2;
        }

      

    }
}
